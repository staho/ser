# ser



## Name
SER or Simple Excel Reporting


## Description
SER is a bounch of VBA scripts to create a structurized (i.e. hierarchical) report, based on data (Excel [tables](#tables)) and report [templates](#templates).


## Working example

### example data tables

Machinery:

|id	|name|
|---|-----|
|1	|milling machine|
|2	|turning machine|

Issues:

|id	|machinery_id    |description|
|---|----------------|------------|
|1	|1               |missing safeguards|
|2	|1               |the emergency stop button located less than 50 cm above the floor|
|3	|2               |the main switch cannot be locked in OFF position|

### required report
```
Machinery Maintenance Report

known issues for milling machine:

- missing safeguards
- the emergency stop button located less than 50 cm above the floor

known issues for turning machine:

- the main switch cannot be locked in OFF position

(end of report)
```

### example template
```
Machinery Maintenance Report

{machinery}known issues for {=machinery.name}:
{issue}- {=issue.description}
{/issue}
{/machinery}
(end of report)
```

## Tables

The data is kept in named Excel tables. (1)

Every data table has a column "id", containing unique identifiers, not necessarily numbers.

Relation with other (parent) tables is defined with a column `parent_ID` or so. Values of the column `parent_ID` refers to identifiers of the tables `parent`. 

E.g. every `issue` refers to `machinery`, therefore the table `issues` contains the column `machinery_id`.

Relation between table names (`machinery`, `issues`), tag names (`machinery`, `issue`) and id names (`machinery_id`, `issue_id`) is defined by a function [flection()](#Flection).

(1) Future improvement: data could be kept as ranges, too. Sheet name would work as table name. Row 1:1 would define column headers. Could be useful for formulas spilling to adjacent cells. 


## Templates

Templates are text files of any type (.txt, .html etc.), having [tags](#tags) of two kinds:
- single tags to be replaced with value, like `{=table.field}`
- pair tags, to mark a template section, like `{table}...{/table}`

Depending on the tag and the [context](#context), tag contents can be:
- omitted (dropped)
- included for further processing
    - once
    - repeated for every data row in the current context

Processing means that the template section (i.e. tag contents) is considered as a template and replaced with the resulting section report.

Having processed a templace section (e.g. `{TABLE1/}`), all the assignements are made for the **current** tag (e.g. `{=TABLE1.field}`). The top level assignements (`{=RAPORT.field}`) are made when the whole template is processed. 

The default template extention is ".html". To change it, modify the "e" variable in the "twórz_raport" sub.

A template can be split into several files. Each file is processed separately, then the results are concatenated into one report. Define the report structure (i. e. the list of basenames of component files) in the field `szablon` (Polish "template") in the table `raporty` ("reports"), separated with commas, e.g. `cover, index, machinery, issues, end`. 


## Tag nesting

Pair tags can be nested. Yet, the **same** tag cannot be nested in itself. 

Example of allowed nesting:
```
{machinery}
    {?issue}
        for the machinery {=machinery.id} there are the following issues:
        {issue}
            - {=issue.description}
        {/issue}
    {/?issue}
    {!issue}
        no issue for the machinery {=machinery.id}
    {/!issue}
{/machinery}
```

Example of wrong nesting:
```
{machinery}
    {?issue}
        {machinery}
            ...
        {/machinery}
    {/?issue}
{/machinery}
```



## Tags

In the list below (and not in actual templates) pair tags `{tag}...{/tag}` are abbreviated to `{tag/}`, e.g. `{!table/}` is an abbreviation of `{!table}...{/!table}`.

### {=table.field}
replace the tag with `table.field` value of the current row

### {table/}  
process (repeat) the tag contents (`...`) for every record in the `table`, meeting [context](#context)

### {?table[.field]/} 
process the tag contents if the `table` contains (in the current context) any rows (or rows with nonempty `field`, if specified) 

### {!table[.field]/} 
process the tag contents if the `table` does not contain any rows (or rows with nonempty `field`) 

### {?=table.field/} 
process the tag contents if `table.field` is not empty 

### {!=table.field/}  
process the tag contents if `table.field` is empty 

### {%table[.field] [pattern%]} ... % ... {/%table[.field]}   
process (repeat) the tag contents (` ... % ... `) for every filename mathing `pattern%`

The `pattern%` is a mask to search files and can (should) contain wild characters `?` and `*`, just like in CMD.EXE. 
The only difference is the `%` character, which is replaced with the value of `table.id` (or `table.field`, if specified). 
E.g. if `table.field` is equal to "123", `*@m%.*jpg` will search for JPG files containing "\@m123." in its name. 

The `%` in the tag contents is replaced with found filename. 
E.g. if the tag contents is `<img href="%">` and the found filenames are "media\1st floor\IMG5463 \@m123.jpg" and "media\3rd floor\IMG9928 \@m123. \@m132.jpg", the result text would be:
```
<img href="media\1st floor\IMG5463 @m123.jpg"><img href="media\3rd floor\IMG9928 @m123. @m132.jpg">
```

Files can be placed in a folder `media` and its subfolders (any depth).

If the `pattern%` is omitted, using the recently specified. It is useful for conditional templates (see below), to avoid repeating same patterns. E.g. the following template will produce "\<div>" only if there are files to be displayed:
```
{?%table.field pattern%}
    <div class="gallery">
        {%table.field}
            <img src="%">
        {/%table.field}
    </div>
{/?%table.field}

```

### {?%table[.field] [pattern%] /}
process the tag contents if there are filenames mathing the `pattern%`

### {!%table[.field] [pattern%] /}
process the tag contents if there is no filename mathing the `pattern%`



## Context

The context is a list of table row identifiers. When a template section (defined by a tag) is processed (repeated) for a given table row, this row identifier is added to the context. 

E.g. processing the template:
```
{machinery} {issue}...{/issue} {/machinery}
```
the section ` {issue}...{/issue} ` is repeated for every `machinery` record, having context: `machinery_id=machinery.id`. 

The subsection `...` will be processed only for the `issues` meeting the condition `issues.machinery_id=machinery.id` and the context will be `machinery_id=machinery.id, issue_id=issues.id`.


## Flection

Tables are sets of rows, so table names tend to have plural form, like "issues". For the tags, we think of an item, so using a singular form (`{issue}...{/issue}`, `{=issue.description}`) is more natural. Yet, some words are uncountable, having the same form for sets and items ("machinery").

There is a function translating one form to another:
```
function flex(ByVal item As String, Optional ByVal form As Integer = FLEX_KEY) As String
```
- `item` is table name, tag name or key name
- `form` is one of FLEX_KEY, FLEX_SET, FLEX_ITEM

E.g. `flex("machinery", FLEX_KEY)` returns "machinery_id".

The function `flex` is based on table `deklinacja` ("flection" in Polish), containing all the required forms. (1)

(1) Future improvement: a few typical form relations could be defined within the "flex()" function itself, without `flection` table. E.g. if the same form is used for tag and table, default forms could be accepted automatically, like "machinery", "machinery", "machinery_id".


## Global values

As for now, the table `Raport` ("report" in Polish) is regarded as the starting point. You can use `{=RAPORT.field}` tags anywhere in the report, i.e. the whole template is considered as enclosed in `{raport/}` tag.

Since `{raport}` is always defined, tag `{!raport/}` can be used as commenting -- its contents will never be processed. 


## Installation
Attach the scripts to the workbook containing data (should be .xlsm) or to any other workbook in the same directory. Run the sub `twórz_szablon` (i.e. "create the report" in Polish). 

In the same directory with the macros workbook, place the templates and related stuff, like all those .css, .jpg etc. for HTML templates.

If the macros are not in the data workbook, the data workbook should be **active** when starting the macro (as it is the active workbook that is processed).

NB. An example workbook should be (and will be) a part of the project, but as for now, I need some time to remove its sensitive data.


## Usage

For template structure, see the exaples above. 

IMHO the best choice is to use HTML templates and then convert HTML to PDF with WeasyPrint or something alike. 

If a particular order is required, add a column "sortowanie" ("sorting" in Polish) to any table -- it will be reordered at the macro start.

## Support

No support is provided. You can try to contact me via GitLab, but I do not promise an answer.


## Roadmap

- Clean up the code. As the project landed here, replacing Polish variables and fuctions with English ones seems necessary.

- Add internationalization. The macro language is for developers, the tables (like `deklinacja`, `raporty`) are for users, though. A simple I10N module could define basic names to be used.

- Add section definitions {~}. For complex templates, split into several files, creating e.g. a contents index requires updating the index template after any change in the template part files. Instead, one could define a section `index` with the tag `{~index/}` in every single file; the template structure would be "cover, ~index, machinery, issues, end", so `~index` would be defined automatically based on the definitions in all the other files. 

- Add recurrency. Having columns `id` and `pid` (for "parent id"), a tree structure can be defined in any table. To represent such a structure in the report, the templace could contain tags `{^table} ... {=^table} ... {/^table}`, where `{=^table}` is to be replaced (recursively) with the tag contents **with** the tag itself. 

## Contributing

If you would like to contribute, contact me via GitLab. 


## Authors and acknowledgment

GetLocalOneDrivePath by Guido Witt-Dörring is a great piece of code translating cloud names (https://...) into local name (c:\...)
https://gist.github.com/guwidoe/038398b6be1b16c458365716a921814d

Aria (AI companion of the Opera browser) and ChatGPT did their job, they will not care, though. 

Author of all other parts: staho.


## License
The project is meant to be as free as possible. 
Just keep a note, that the program homepage is https://iterum.biz/ser.

## Project status
The code is ugly-bugly, as it is just made for our current job. Hopefully it will be better and better. 

***


# original gitlab README, left for later reading ;-)


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/staho/ser.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/staho/ser/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

***
