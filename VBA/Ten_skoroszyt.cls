VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Ten_skoroszyt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True
Option Explicit

' nazwy kolumn tabeli "deklinacje"
Const przyTAG = "tag"
Const przyTAB = "tabela"
Const przyID = "id_"

Function podstaw(raport As String, tablica As String, wiersz As Long) As String
' zmienia znaczniki {=tablica.pole} na cell(tablica, pole, wiersz)
' trzeba przekazywa� numer wiersza, bo VBA nie przewiduje struktur odpowiadaj�cych wierszowi tabeli
    Dim wzorzec As String
    Dim podzial As Object
    Dim przedRaport As String, kolumna As String, zaRaport As String
    
    wzorzec = "^([\s\S]*)\{\=" & flex(tablica, FLEX_ITEM) & "\.([\w����󜟿A�ʣ�ӌ��]*)( [^}]*)?\}([\s\S]*)$"
    
    Set podzial = dopasuj(wzorzec, raport)
    While podzial.Count > 0
        With podzial.item(0)
            przedRaport = .submatches(0)
            kolumna = .submatches(1)
            zaRaport = .submatches(3)
            raport = przedRaport & formatuj(cell(tablica, kolumna, wiersz)) & zaRaport
        End With
        Set podzial = dopasuj(wzorzec, raport)
    Wend
    podstaw = raport
End Function

Function podstaw_wg_id(raport As String, tablica As String, id As Variant) As String
    Dim i As Long
    
    For i = 1 To table_length(tablica)
        If cell(tablica, "id", i) = id Then
            podstaw_wg_id = podstaw(raport, tablica, i)
            Exit Function
        End If
    Next i
    
    ojoj "w tabeli " & tablica & " nie znaleziono id=" & id
End Function

Function wiersz_pasuje(dane As ListObject, ByVal wiersz As Long) As Boolean
' sprawdza, czy dany wiersz tabeli 'dane' pasuje do kontekstu
    
    Dim j As Integer, k As Integer
    
    For j = bufl - 1 To 0 Step -1
        ' je�li w tabeli 'dane' jest taka kolumna
        For k = 1 To dane.ListColumns.Count
            If dane.ListColumns(k).Name = bufk(j) Then
                If dane.ListColumns(k).DataBodyRange(wiersz) = bufv(j) Then
                    Exit For ' ten warunek spe�niony, idziemy do nast�pnego
                Else
                    ' warunek nie spe�niony
                    wiersz_pasuje = False
                    Exit Function
                End If
            End If
        Next k
        ' nie znaleziono odpowiedniej kolumny, przechodzimy do nast�pnego warunku
    Next j
    
    ' przeszli�my wszystkie warunki (spe�nione lub nie dotycz�)
    wiersz_pasuje = True
End Function

Function istnieje(nazwa_tabeli As String, Optional kolumna As String = "") As Boolean
' sprawdza, czy w tabeli 'tablica' istnieje wiersz pasuj�cy do kontekstu
' je�li okre�lono 'kolumn�', warto�� musi by� <> ""
    
    Dim i As Integer, j As Integer, k As Integer
    
    Dim dane As ListObject
    Set dane = tabela(nazwa_tabeli)
    
    ' sprawdzamy wiersze po kolei -- do pierwszego trafienia
    For i = 1 To table_length(nazwa_tabeli)
        If wiersz_pasuje(dane, i) Then
            ' je�li nie okre�lono kolumny, sprawdzamy tylko istnienie odpowiedniego wiersza
            If kolumna = "" Then
                istnieje = True
                Exit Function
            ' a je�li okre�lono, warto�� musi by� niepusta (r�na od "")
            ElseIf cell(nazwa_tabeli, kolumna, i) <> "" Then
                istnieje = True
                Exit Function
            End If
        End If
    Next i
    
    ' sprawdzili�my wszystkie wiersze tabeli, nie znaleziono pasuj�cego
    istnieje = False
End Function

Function bie��cy(tablica As String, kolumna As String) As Variant
' zwraca warto�� podanego pola w bie��cym kontek�cie
    Dim w As Integer, i As Integer
    ' znajd� 'tablic�' w buforze
    For i = 0 To bLen - 1
        If flex(bKey(i), FLEX_SET) = flex(tablica, FLEX_SET) Then
            w = i
        End If
    Next i
    bie��cy = wgId(tablica, bVal(w), kolumna)
End Function

Function niepuste(tablica As String, kolumna As String, id As Variant) As Boolean
    Dim i As Long
    For i = 1 To table_length(tablica)
        If cell(tablica, "id", i) = id Then
            niepuste = cell(tablica, kolumna, i) <> ""
            Exit Function
        End If
    Next i
End Function

Sub wyszukaj_pliki(ByVal nazwaTabeli As String, ByVal nazwaKolumny As String, ByVal wzorzec As String)
    Dim x As Variant
    
    ' wywo�anie z pustym 'wzorcem' => korzystaj z poprzedniego wyszukiwania
    If wzorzec = "" Then Exit Sub
    
    x = bValByKey(flex(nazwaTabeli, FLEX_KEY))
    ' je�li okre�lono kolumn� inn� ni� id
    If nazwaKolumny <> "" Then _
        x = wgId(nazwaTabeli, x, nazwaKolumny)
        
    ' % zmienione na warto�� tabela.kolumna wg kontekstu
     wzorzec = Replace(wzorzec, "%", x)
    
    ' szukamy plik�w zgodnych z wzorcem
    ffind "media", wzorzec

End Sub

Function kolejny_znacznik(text As String) As String
' zwraca nazw� pierwszego znacznika w �a�cuchu 'text'
' je�li nie ma znacznik�w, zwraca ""
' wzorzec znacznika okre�lono poni�ej:

    ' Wzorzec do dopasowania
    Const pattern = "\{(([?!]?%?|[?!]=)[\w����󜟿A�ʣ�ӌ��]+(\.[\w����󜟿A�ʣ�ӌ��]+)?)( [^}]*)?\}"

    Dim matches As Object
    Set matches = dopasuj(pattern, text, False)

    If matches.Count > 0 Then
        kolejny_znacznik = matches.item(0).submatches(0)
    Else
        kolejny_znacznik = ""
    End If
    'echo kolejny_znacznik, 1
    'If Left(kolejny_znacznik, 1) = "?" Then MsgBox kolejny_znacznik
End Function

Function raport(ByVal szablon As String, pkey As String, pval As Variant) As String
' znajd� w 'szablonie' pary znacznik�w postaci "{[?!]tag}...{/[?!]tag}"
' zawarto�� pomi�dzy znacznikami:
' - je�li znacznik zaczyna si� od "?" lub "!", je�li istniej� (?) lub nie istniej� (!) warto�ci okre�lone znacznikiem,
'   -- przetwarzaj dalej, w przeciwnym razie pomi�
' - powiel tyle razy, ile jest odpowiednich rekord�w danych
    Dim przedSzablon As String, �r�dSzablon As String, zaSzablon As String
    Dim tag As String, przedTag As String, �r�dTag As String, zaTag As String, poTag As String
    Dim dane As ListObject
    Dim element As String, dodaj As Boolean
    Dim i As Long, x As Variant, t As String
    
    Dim msg As String, tags As String
    
    pkey = flex(pkey, FLEX_KEY)
    bAdd pkey, pval
    msg = String(bLen, ">") & "raport" & "(" & pkey & "=" & pval & ")"
    echo msg, bLen
    
    
    raport = ""
    tags = " "
    tag = kolejny_znacznik(szablon)
    While tag <> ""
        echo msg & tags & UCase(tag), bLen
        
        With dopasuj("^([\s\S]*?)\{" & regexp(tag) & "( ([^}]*))?\}([\s\S]*?)\{\/" & regexp(tag) & "\}([\s\S]*)$", szablon)
            przedSzablon = .item(0).submatches(0)
            poTag = .item(0).submatches(2)
            �r�dSzablon = .item(0).submatches(3)
            zaSzablon = .item(0).submatches(4)
        End With
        
        ' dodaj bez zmian fragment przed znacznikiem otwieraj�cym
        raport = raport & przedSzablon
        
        ' rozbi�r syntaktyczny znacznika
        With dopasuj("^([?!]?%?|[?!]=)?([\w����󜟿A�ʣ�ӌ��]+)(\.(.+))?$", tag)
            przedTag = .item(0).submatches(0)
            �r�dTag = .item(0).submatches(1)
            zaTag = .item(0).submatches(3)
        End With
        
        If przedTag = "" Then
        
        ' zwyk�y znacznik -- powielenie tekstu mi�dzy znacznikami
            t = flex(�r�dTag, FLEX_SET)
            Set dane = tabela(t)
            For i = 1 To table_length(t)
                If wiersz_pasuje(dane, i) Then
                    If zaTag = "" Then
                        dodaj = True
                    ElseIf cell(t, zaTag, i) <> "" Then
                        dodaj = True
                    Else
                        dodaj = False
                    End If
                    If dodaj Then
                        echo Int(i * 100 / table_length(t)) & "%", bLen, -1
                        element = raport(�r�dSzablon, �r�dTag, cell(t, "id", i))
                        raport = raport & element
                    End If
                End If
            Next i
            echo "", bLen, -1
            
        ElseIf (przedTag = "?") Or (przedTag = "!") Then
        
        ' znaczniki warunkowe -- je�li istnieje warto�� w tabeli
            dodaj = False
            If istnieje(flex(�r�dTag, FLEX_SET), zaTag) Then
                If przedTag = "?" Then _
                    dodaj = True
            Else
                If przedTag = "!" Then _
                    dodaj = True
            End If
            
            If dodaj Then _
                raport = raport & raport(�r�dSzablon, pkey, pval)
            
        ElseIf (przedTag = "?=") Or (przedTag = "!=") Then
        
        ' znaczniki warunkowe -- je�li dana warto�� jest niepusta
            dodaj = False
            If bie��cy(flex(�r�dTag, FLEX_SET), zaTag) <> "" Then
                If przedTag = "?=" Then _
                    dodaj = True
            Else
                If przedTag = "!=" Then _
                    dodaj = True
            End If
            If dodaj Then _
                raport = raport & raport(�r�dSzablon, pkey, pval)
                
        ElseIf (przedTag = "%") Then
        
        ' wype�nianie znalezionymi plikami
            wyszukaj_pliki �r�dTag, zaTag, poTag
            For i = 0 To foundn - 1
                element = Replace(�r�dSzablon, "%", foundf(i))
                raport = raport & element
            Next i
            
        ElseIf (przedTag = "?%") Or (przedTag = "!%") Then
        
        ' znaczniki warunkowe -- je�li (nie) istniej� pliki pasuj�ce do wzorca
            wyszukaj_pliki flex(�r�dTag, FLEX_SET), zaTag, poTag
            If (foundn > 0) = (przedTag = "?%") Then _
                raport = raport & raport(�r�dSzablon, pkey, pval)
        
        Else
            ojoj "nieznany znacznik: " & tag
        End If
        
        ' usuni�cie z szablonu przetworzonego tekstu
        szablon = zaSzablon
        
        tags = tags & tag & " "
        tag = kolejny_znacznik(szablon)
    Wend
    
    raport = raport & szablon
    raport = podstaw_wg_id(raport, flex(pkey, FLEX_SET), pval)
    
    echo "", bLen
    bOff
End Function

Sub tw�rz_raport()
    Dim szablon As Variant, szablony As String
    Dim s As String, r As String, nazwa As String, e As String
    Dim i As Integer, n As Integer
    Dim raporty As String
    Dim start As Single: start = Timer
    
    'Set data_file = ActiveWorkbook
    sortowanie
    raporty = "raporty"
    
    For i = 1 To table_length(raporty)
        If cell(raporty, "generuj", i) <> "" Then
            table_init
            zakres("obszar").Value = i
            'Application.Calculation = xlCalculationManual
            
            e = ".html"
            r = ""
            szablony = cell(raporty, "szablon", i)
            For Each szablon In Split(szablony, ",")
                szablon = Trim(szablon)
                echo "ODCZYTYWANIE SZABLONU " & Replace(szablony, szablon, UCase(szablon))
                s = wczytaj(GetLocalPath(ThisWorkbook.path) & "\" & szablon & e)
                echo "TWORZENIE RAPORTU " & cell(raporty, "plik", i) & " [" & i & "] " & Replace(szablony, szablon, UCase(szablon))
                r = r & raport(s, "raport", cell(raporty, "id", i))
            Next szablon
            echo "ZAPISYWANIE RAPORTU " & i
            nazwa = cell(raporty, "plik", i)
            If nazwa = "" Then nazwa = "raport" & i
            zapisz r, GetLocalPath(ActiveWorkbook.path) & "\" & nazwa
            
            Application.Calculation = xlCalculationAutomatic
            n = n + 1
        End If
    Next i
    
    echo "Zako�czono. Utworzone raporty: " & n & " w czasie " & Int(Timer - start) & " s"
    Beep
    Beep
End Sub


