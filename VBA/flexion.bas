Attribute VB_Name = "flexion"
' FLEXION -- obs�uga tabel z formami nazw
'
' wykorzystywane modu�y i procedury:
'   TABLES.cell
'   POMOCNICZE.ojoj
'
' tabela deklinacja:
'   key:        set:    item:
'   id_maszyny  maszyny maszyna
'   id_strefy   strefy  strefa
'   typ_id      typy    typ
'   ...
'
' flex_dict: id_maszyny=1, id_strefy=2, typ_id=3, maszyny=1, strefy=2, typy=3, maszyna=1, strefa=2, typ=3
' flex_keys: 1=id_maszyny, 2=id_strefy, 3=typ_id
' flex_sets: 1=maszyny, 2=strefy, 3=typy
' flex_items: 1=maszyna, 2=strefa, 3=typ
' flex_rows=3


Global Const FLEX_KEY = 0
Global Const FLEX_SET = 1
Global Const FLEX_ITEM = 2

Private flex_dict As Object
Private flex_keys(100) As String
Private flex_sets(100) As String
Private flex_items(100) As String
Private flex_rows As Integer


Private Sub add_flex(item As String, row As Integer)
' dodaje element do s�ownika flex_dict, sprawdzaj�c duplikaty
' dopuszczalne s� jednakowe nazwy w tym samym wierszu
    
    Dim prev As Integer
    
    If flex_dict.exists(item) Then
        prev = flex_dict(item)
        If prev <> flex_rows Then _
            ojoj "duplikat �" & item & "� w wierszach " & prev & " i " & row
    Else
        flex_dict.Add item, row
    End If
End Sub

Private Sub load_flex()
' inicjuje s�ownik flex_dict

    Dim i As Integer
    Dim t As ListObject
    
    flex_rows = 0
    Set flex_dict = CreateObject("Scripting.Dictionary")
    For i = 1 To table_length("deklinacja")
        flex_keys(flex_rows) = cell("deklinacja", "id_", i)
        flex_sets(flex_rows) = cell("deklinacja", "tabela", i)
        flex_items(flex_rows) = cell("deklinacja", "tag", i)
        add_flex flex_keys(flex_rows), flex_rows
        add_flex flex_sets(flex_rows), flex_rows
        add_flex flex_items(flex_rows), flex_rows
        flex_rows = flex_rows + 1
    Next i
    
End Sub

Function flex(ByVal item As String, Optional ByVal form As Integer = FLEX_KEY) As String
' zwraca ��dan� form� gramatyczn�

    If flex_dict Is Nothing Then load_flex
    
    Dim k As Integer
    If Not flex_dict.exists(item) Then
        ojoj "nie okre�lono odmiany dla �" & item & "�"
    Else
        k = flex_dict(item)
    End If
    
    Select Case form
    Case FLEX_KEY
        flex = flex_keys(k)
    Case FLEX_SET
        flex = flex_sets(k)
    Case FLEX_ITEM
        flex = flex_items(k)
    Case Default
        ojoj "nie zdefiniowano formy " & form & " (" & item & ")"
    End Select
End Function

Private Sub test()
    Dim k
    Debug.Print flex("maszyna", FLEX_SET)
    For Each k In flex_dict.Keys
        Debug.Print k, flex(k, FLEX_KEY), flex(k, FLEX_SET), flex(k, FLEX_ITEM)
    Next k
End Sub
