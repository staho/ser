Attribute VB_Name = "katalogi"
Option Explicit

Public foundf(100) As String
Public foundn As Integer

Private last As String
Private ext As Variant ' pomijamy pliki, kt�rych nie wykorzystamy
Private pwd As String
Private pomi� As Integer


Private Sub follow(ByVal root As String)
' kontynuuje szukanie rozpocz�te przez ffind

    Dim d As Object
    Dim f As String
    Dim i As Integer
    
    ' wyszukujemy w bie��cym katalogu
    ' uwaga! d�ugo�� root&last nie mo�e przekroczy� 260 znak�w -- ograniczenie funkcji Dir()
    f = Dir$(root & "\" & last)
    While f <> ""
        For i = LBound(ext) To UBound(ext)
            If UCase(Right(f, Len(ext(i)))) = ext(i) Then
                foundf(foundn) = root & "\" & f
                foundn = foundn + 1
                Exit For
            End If
        Next i
        f = Dir$
    Wend
    
    ' przegl�damy podkatalogi
    For Each d In CreateObject("Scripting.FileSystemObject").GetFolder(root).SUBFOLDERS
        follow d
    Next d

End Sub

Sub ffind(ByVal root As String, ByVal mask As String)
' szuka w katalogu 'root' i podkatalogach plik�w o nazwach zgodnych z 'mask'
' wype�nia tablic� foundf() nazwami plik�w, ilo�� plik�w zachowuje w foundn
' wywo�anie z root="" powoduje ustawienie warto�ci pocz�tkowych dla zmiennych statycznych

    Dim i As Integer
    Static kolejne As Boolean
    
    If Not kolejne Then
        last = ""
        ext = Array(".JPG", ".PNG")
        pwd = GetLocalPath(ActiveWorkbook.path)
        pomi� = Len(pwd) + 2
        kolejne = True
    End If
    
    ' poszukiwania tego samego powinny da� ten sam wynik
    If mask = last Then Exit Sub
    
    ' pocz�tek nowego wyszukiwania
    last = mask
    foundn = 0
    ' szukamy tylko wzgl�dem pwd
    follow pwd & "\" & root
    
    ' 'root' jest podkatalogiem 'pwd' - obcinamy pocz�tkowe len(pwd)+1 znak�w
    ' zmieniamy "\" na "/", "#" na %23, " " na %20
    For i = 0 To foundn - 1
        foundf(i) = Replace(Replace(Replace(Mid(foundf(i), pomi�), "\", "/"), "#", "%23"), " ", "%20")
    Next i
End Sub

