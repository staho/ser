Attribute VB_Name = "kontekst"
Option Explicit

''''''''''''''''''''''''''''''''''''''''''
'   kontekst
'
'   czyli stos odwo�a� do tabel z danymi
'   na ka�dym poziomie odwo�ania pami�tane s�
'   - tabela (nazwa)
'   - warto�� 'id' dla bie��cego wiersza
'   dodatkowo pami�tany jest aktualny poziom odwo�ania

Public bufk(100) As String
Public bufv(100) As Variant
Public bufl As Integer


Sub bPrint()
    Dim i As Integer
    For i = 0 To bufl - 1
        Debug.Print bufk(i) & " = " & bufv(i)
    Next i
End Sub

Function bLen() As Integer
    bLen = bufl
End Function

Sub bAdd(key As String, val As Variant)
    bufk(bufl) = key
    bufv(bufl) = val
    bufl = bufl + 1
End Sub

Function bGet(ByVal item As String, Optional ByVal level As Integer = -1) As Variant
    If level = -1 Then _
        level = bufl
    Select Case item
    Case "key"
        bGet = bufk(level)
    Case "val"
        bGet = bufv(level)
    Case Else
        bPrint
        ojoj "bufor nie przechowuje danych typu " & item
    End Select
End Function

Sub bOff(Optional levels As Integer = 1)
    bufl = bufl - levels
End Sub

Function bKey(Optional ByVal level As Integer) As String
    bKey = bGet("key", level)
End Function

Function bVal(Optional ByVal level As Integer) As Variant
    bVal = bGet("val", level)
End Function

Function bValByKey(ByVal key As String) As Variant
    Dim i As Integer
    For i = 0 To bLen - 1
        If bKey(i) = key Then
            bValByKey = bVal(i)
            Exit Function
        End If
    Next i
    bPrint
    ojoj "kontekst nie zawiera klucza """ & key & """"
End Function



