Attribute VB_Name = "pomocnicze"
Option Explicit

Dim data_file As Object


''''''''''''''''''''''''''''''''''''''''''
'   przebieg programu
'

Sub echo(s As String, Optional poni�ej As Integer = 0, Optional obok As Integer = 0)
    Static status As Object
    If status Is Nothing Then _
        Set status = zakres("status")
    
    'If s <> "" Then Debug.Print s
    status.Offset(poni�ej, obok).Value = s
End Sub

Function ojoj(Optional komentarz As String = "")
' przerywa wykonanie programu z komunikatem
    echo "#NIEDOBRZE " & komentarz
    If komentarz <> "" Then
        MsgBox komentarz
    End If
    End
End Function



''''''''''''''''''''''''''''''''''''''''''
'   narz�dzia tekstowe
'

Function crlf() As String
    crlf = Chr(13) & Chr(10)
End Function

Function regexp(ByVal napis As String) As String
' poprzedza uko�nikiem znaki specjalne
    ' Lista znak�w specjalnych do zamiany
    ' "\" musi by� pierwszy, bo algorytm si� wysypie
    Dim specjalneZnaki As String: specjalneZnaki = "\.*+?^$(){}[]|"
    
    Dim i As Integer, znak As String
    For i = 1 To Len(specjalneZnaki)
        znak = Mid(specjalneZnaki, i, 1)
        napis = Replace(napis, znak, "\" & znak)
    Next i
    regexp = napis
End Function

Function dopasuj(pattern As String, text As String, Optional all As Boolean = False) As Object
' zwraca wynik dopasowania wyra�enia regularnego 'pattern' do napisu 'text'

    ' Tworzenie obiektu RegExp
    Dim r As Object
    Set r = CreateObject("VBScript.RegExp")
    ' Ustawienie w�a�ciwo�ci obiektu RegExp
    With r
        .Global = all ' Dopasuj wszystkie wyst�pienia wzorca
        .IgnoreCase = True ' Ignoruj wielko�� liter
        .MultiLine = True
        .pattern = pattern ' Ustawienie wzorca
    End With
    ' Dopasowanie wzorca w tek�cie
    Set dopasuj = r.Execute(text)
End Function

Function konieckropka(wiersz As String, Optional kropka As String = ".") As String
    Dim kropki As String: kropki = ".,;"
    Dim dwukropki As String: dwukropki = ":?!"
    
    Dim q As String: q = Mid(wiersz, Len(wiersz))
    If InStr(dwukropki, q) Then
        konieckropka = wiersz
        Exit Function
    End If
    If InStr(Mid(wiersz, Len(wiersz)), kropki) Then
        konieckropka = Left(wiersz, Len(wiersz) - 1)
    Else
        konieckropka = wiersz
    End If
    konieckropka = konieckropka & kropka
End Function

Function formatuj(tekst As String, Optional zdaniami As Boolean = False, Optional sep As String) As String
' dla ka�dego wiersza (fragmentu tekstu rozdzielonego separatorem sep):
' - usuwa bia�e znaki z przodu i z ty�u
' - zamienia spacje po pojedynczych znakach (a i o u w z) na nbsp;
' - je�li wiersz zaczyna si� od litery,
'   -- zmienia pierwsz� liter� na wielk�;
'   -- je�li wiersz nie ko�czy si� dwukropkiem, na ko�cu umieszcza kropk�
' - je�li wiersz nie zaczyna si� od litery, na ko�cu umieszcza przecinek
' na ko�cu tekstu umieszcza kropk�
' ka�dy wiersz otacza znacznikiem tag (podstawia wiersz w miejsce znaku %)

    Dim znaki() As Variant: znaki = Array("a", "i", "o", "u", "w", "z")
    
    Dim wiersze() As String
    Dim i As Integer, j As Integer
    Dim s As String, p As String, q As String
    
    If IsMissing(sep) Then
        sep = Chr(10)
    ElseIf sep = "" Then
        sep = Chr(10)
    End If
    
    wiersze = Split(tekst, sep)
    
    For i = LBound(wiersze) To UBound(wiersze)
        s = Trim(wiersze(i))
    
        ' czy�cimy wiersz
        For j = LBound(znaki) To UBound(znaki)
            s = Replace(s, " " & znaki(j) & " ", " " & znaki(j) & Chr(160))
        Next j
        p = Left(s, 1)
        q = Mid(s, Len(s))
        
        If zdaniami Then
            If UCase(p) = LCase(p) Then
            ' znak niewra�liwy na zmian� wielko�ci tzn. nie litera
                
                If i = UBound(wiersze) Then
                    s = konieckropka(s)
                Else
                    s = konieckropka(s, ",")
                End If
            Else
            ' litera
                s = UCase(Left(s, 1)) & Mid(s, 2)
                s = konieckropka(s)
            End If
        End If
        
        If formatuj = "" Then
            formatuj = s
        Else
            formatuj = formatuj & sep & s
        End If
    Next i
    
End Function


''''''''''''''''''''''''''''''''''''''''''
'   Excel -- dost�p do danych
'

Function zakres(nazwa As String) As Range
    Dim ws As Worksheet
    If data_file Is Nothing Then Set data_file = ActiveWorkbook
    
    On Error Resume Next
    For Each ws In data_file.Worksheets
        Set zakres = ws.Range(nazwa)
        If Not zakres Is Nothing Then
            On Error GoTo 0
            Exit Function
        End If
    Next ws
    On Error GoTo 0
End Function

Function tabela(nazwa As String, Optional wymagana As Boolean = True) As ListObject
' zwraca tabel� o nazwie 'nazwa' z aktywnego skoroszynu Excel
' tabela mo�e znajdowa� si� w dowolnym arkuszu
    'metoda 1
    Dim ws As Worksheet
    If data_file Is Nothing Then Set data_file = ActiveWorkbook
    
    For Each ws In data_file.Worksheets
        For Each tabela In ws.ListObjects
            If tabela.Name = nazwa Then
                ' je�li istnieje kolumna "sortowanie", uporz�dkuj tabel�
                Exit Function
            End If
        Next tabela
    Next ws
    
    If wymagana Then
        ojoj "brak tabeli " & nazwa
    Else
        Set tabela = Nothing
    End If
End Function


Sub sortowanie()
    Const SORT_COL = "sortowanie"
    Dim ws As Worksheet
    Dim sort As Object
    Dim tbl As ListObject
    Dim i As Integer, n As Integer
    If data_file Is Nothing Then Set data_file = ActiveWorkbook
    Dim calc As XlCalculation
    
    calc = Application.Calculation
    Application.Calculation = xlCalculationManual
    
    For Each ws In data_file.Worksheets
        echo "SORTOWANIE:"
        For Each tbl In ws.ListObjects
            n = n + 1
            echo tbl.Name, n
            Set sort = Nothing
            On Error Resume Next
            Set sort = tbl.ListColumns(SORT_COL).Range
            On Error GoTo 0
            If Not sort Is Nothing Then
                echo "sortowanie " & UCase(tbl.Name), n
                With tbl.sort
                    .SortFields.Clear
                    .SortFields.Add key:=sort, _
                        SortOn:=xlSortOnValues, Order:=xlAscending
                    .Header = xlYes
                End With
                Application.Calculation = xlAutomatic
                tbl.sort.Apply
                Application.Calculation = xlCalculationManual
            End If
        Next tbl
    Next ws
    
    For i = 1 To n
        echo "", i
    Next i
    echo ""
    
    Application.Calculation = calc
End Sub

Function wgId(tabela As String, ByVal id As Variant, Optional ByVal kolumna As String) As Variant
' zwraca numer wiersza, dla kt�rego kolumna "id" zawiera warto�� 'id'
' je�li okre�lono 'kolumn�', zwraca jej warto�� w tym wierszu
    Dim i As Long
    For i = 1 To table_length(tabela)
        If cell(tabela, "id", i) = id Then
            If IsMissing(kolumna) Then
                wgId = i
            Else
                wgId = cell(tabela, kolumna, i)
            End If
            Exit Function
        End If
    Next i
    
    ojoj "nie znaleziono wiersza " & tabela & "[id=" & id & "]"
End Function

'Function tabela2(nazwa As String) As ListObject
'    'metoda 2
'    On Error Resume Next
'    Set tabela = Application.Range(nazwa)
'    On Error GoTo 0
'    If Not tabela Is Nothing Then
'        Set tabela = tabela.ListObject
'    Else
'        MsgBox "nieznana tabela �" & UCase(nazwa) & "�"
'        End
'    End If
'    Exit Function
'
'End Function




''''''''''''''''''''''''''''''''''''''''''
'   I/O
'

Function XXXplik(nazwa As String) As String
' GetLocalPath � konwertuje adres OneDrive/SharePoint na lokalny
' �r�d�o: https://gist.github.com/guwidoe/038398b6be1b16c458365716a921814d
'
    plik = GetLocalPath(ThisWorkbook.path) & "\" & nazwa
    Debug.Print "nazwa pliku:"
    Debug.Print Chr(9) & "<< " & ThisWorkbook.path & "/"; nazwa
    Debug.Print Chr(9) & ">> " & plik
End Function

Sub zapisz(raport As String, nazwa As String)
    Dim stream As Object
    Dim c As String
    
    Debug.Print "zapisywanie do pliku " & nazwa
    
    'nazwa = plik(nazwa)
    Debug.Print "  >> " & nazwa
    
    ' Utworzenie obiektu strumienia
    Set stream = CreateObject("ADODB.Stream")
    
    ' Konfiguracja strumienia
    With stream
        .Type = 2 ' Typ tekstu
        .Charset = "UTF-8" ' Kodowanie pliku
        .Open
        .WriteText raport, 1 ' Zapisanie zawarto�ci do strumienia
        ' Zapisanie strumienia do pliku
        .SaveToFile nazwa, 2 ' 2 oznacza zapis w formacie UTF-8
        .Close
    End With
    'c = "cmd /c start ""tytu�"" """ & n & """"
    'Shell c, vbNormalFocus
    Debug.Print "  >> utworzono plik " & nazwa
End Sub

Function wczytaj(nazwa As String) As String
' zwraca zawarto�� pliku tekstowego o podanej nazwie

    Dim f As Object
    
    'nazwa = plik(nazwa)
    Set f = CreateObject("ADODB.Stream")
    
    On Error GoTo NieMa
    With f
        .Charset = "utf-8"
        .Open
        .LoadFromFile nazwa
        wczytaj = .ReadText(.size)
    End With
    Set f = Nothing
    Exit Function
    
NieMa:
    ojoj "nie uda�o si� otworzy� pliku: " & nazwa
    wczytaj = ""
    Exit Function
End Function

'Function wczytaj_szablon() As String
'    'poprzednie metody
'    wczytaj_szablon = "RAPORT" & crlf & _
'        "{maszyny}opis maszyny {=maszyny.nazwa}" & crlf & _
'        "   {obserwacje}" & crlf & _
'        "       {=obserwacje.opis}" & crlf & _
'        "   {/obserwacje}" & crlf & _
'        "{/maszyny}" & crlf & _
'        "Index" & crlf & _
'        "{maszyny}{=maszyny.nazwa}" & crlf & _
'        "{/maszyny}" & crlf & _
'        "cbdo"
'    Dim arkusz As Object
'    Dim ostatni As Long
'    Dim wiersz As Range
'
'    wczytaj_szablon = ""
'    Set arkusz = Worksheets("szablon")
'    ostatni = arkusz.Cells(arkusz.Rows.Count, 1).End(xlUp).row
'    For Each wiersz In arkusz.Range("A1:A" & ostatni)
'        wczytaj_szablon = wczytaj_szablon & wiersz.Value & crlf
'    Next wiersz
'    wczytaj_szablon = Trim(wczytaj_szablon)
'End Function

