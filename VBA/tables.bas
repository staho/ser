Attribute VB_Name = "tables"
' TABLES -- buforowanie danych z arkusza Excel
'
' wykorzystywane modu�y i procedury:
'   POMOCNICZE.tabela
'   POMOCNICZE.ojoj

Private table_data(100000) As Variant       ' warto�ci
Private table_ptr As Long                   ' pierwszy wolny element w table_data == ilo�� element�w wykorzystanych
Private table_rows As Object                ' s�ownik <tabela> => ilo�� wierszy
Private table_pos As Object                 ' s�ownik <tabela>.<kolumna> => pozycja w table_data

Public Sub table_init()
    Set table_pos = Nothing
    Set table_rows = Nothing
    table_ptr = 0
End Sub

Public Function table_length(table As String) As Long
' zwraca ilo�� wierszy w tablicy
    
    If table_pos Is Nothing Then
        Set table_pos = CreateObject("scripting.dictionary")
        Set table_rows = CreateObject("scripting.dictionary")
    End If
    
    If Not table_rows.exists(table) Then
        Dim t As ListObject
        Set t = tabela(table)
        table_rows.Add table, t.ListRows.Count
    End If
    
    table_length = table_rows(table)
End Function

Public Function cell(table As String, col As String, ByVal row As Long) As Variant
' zwraca kom�rk� tabeli
' row liczone od 1 do table_length(table)

    Dim t As ListObject
    Dim i As Long
    Dim time As Variant 'Double, msg As String

    If table_pos Is Nothing Then
        Set table_pos = CreateObject("scripting.dictionary")
        Set table_rows = CreateObject("scripting.dictionary")
    End If
    
    If Not table_pos.exists(table & "." & col) Then
        ' pierwsze odwo�anie do table.col -- pobranie danych
        time = Timer
        Set t = tabela(table)
        table_pos.Add table & "." & col, table_ptr
        On Error GoTo NoCol
        For i = 1 To table_length(table)
            table_data(table_ptr) = t.ListColumns(col).DataBodyRange(i)
            table_ptr = table_ptr + 1
        Next i
        On Error GoTo 0
        time = 0.001 * Int(1000000 * (Timer - time))
        If time = 0 Then
            time = ""
        Else
            time = ", " & time & " ms"
        End If
        Debug.Print "wczytano " & table & "." & col & " (" & table_length(table) & " rekord�w" & time & ")"
    End If
    cell = table_data(table_pos(table & "." & col) + row - 1)
    Exit Function
NoCol:
    ojoj "problem z odczytem kolumny " & UCase(table) & "." & UCase(col)
End Function

Sub test()
    Dim i As Integer
    Set table_pos = Nothing
    For i = 1 To table_length("raporty")
        Debug.Print i, cell("raporty", "id", i), cell("raporty", "generuj", i)
    Next
End Sub

